import axios from 'axios';
import jwt_decode from 'jwt-decode';

const instance = axios.create({
  baseURL: 'http://localhost:5000/api',
});

instance.interceptors.response.use(
  (res) => res.data,
  (err) => {
    if (!err.response) {
      return Promise.reject({ network: 'Network Error' });
    }
    return Promise.reject(err.response.data);
  }
);

export default instance;

export const setAuthToken = (token) => {
  if (token) {
    // Apply to every request
    axios.defaults.headers.common['Authorization'] = token;
  } else {
    // Delete auth header
    delete axios.defaults.headers.common['Authorization'];
  }
};

export const getTokenPayload = (token) => {
  const payload = jwt_decode(token);
  const { id, name, email, date } = payload;
  const currentUser = {
    id,
    name,
    email,
    date,
  };
  return currentUser;
};
