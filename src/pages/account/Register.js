import React, { useState } from 'react';
import { Section, Row, Col, TextInput, Icon, Button } from 'react-materialize';
import history from './../../history';
import { LOGIN_URL } from './../../routes';
import gateway from './../../api/gateway';

export default function Register() {
  const [formValues, setFormValues] = useState(defaultFormValues);
  const [errors, setErrors] = useState(defaultErrorsValues);

  const onRegisterClick = async () => {
    try {
      await gateway.post('/users/register', formValues);
      //Redirect to Login Page
      history.push(LOGIN_URL);
    } catch (err) {
      setErrors(err);
    }
  };

  const handleChange = (data) => {
    setFormValues({ ...formValues, ...data });
  };

  return (
    <Section className="container">
      <Row>
        <Col
          s={12}
          m={4}
          xl={4}
          offset="m4 xl4"
          className="z-depth-4 card-panel border-radius-6 register-card bg-opacity-8"
        >
          <div className="register-form">
            <Row>
              <h4 className="header center-align">Register</h4>
            </Row>
            <Row>
              <TextInput
                id="name"
                className={errors.name ? 'invalid' : ''}
                error={errors.name}
                icon={<Icon>assignment_ind</Icon>}
                label="Name"
                s={12}
                value={formValues.name}
                onChange={(e) => {
                  handleChange({ name: e.target.value });
                }}
              />
            </Row>
            <Row>
              <TextInput
                id="email"
                className={errors.email ? 'invalid' : ''}
                error={errors.email}
                icon={<Icon>email</Icon>}
                label="Email"
                s={12}
                value={formValues.email}
                onChange={(e) => {
                  handleChange({ email: e.target.value });
                }}
              />
            </Row>
            <Row>
              <TextInput
                id="password"
                className={errors.password ? 'invalid' : ''}
                error={errors.password}
                type="password"
                icon={<Icon>lock_outline</Icon>}
                label="Password"
                s={12}
                value={formValues.password}
                onChange={(e) => {
                  handleChange({ password: e.target.value });
                }}
              />
            </Row>
            <Row>
              <TextInput
                id="password2"
                className={errors.password2 ? 'invalid' : ''}
                error={errors.password2}
                type="password"
                icon={<Icon>lock_outline</Icon>}
                label="Repeat Password"
                s={12}
                value={formValues.password2}
                onChange={(e) => {
                  handleChange({ password2: e.target.value });
                }}
              />
            </Row>
            <Row>
              <Col s={12} className="input-field">
                <Button
                  waves="light"
                  className="col s12"
                  onClick={onRegisterClick}
                >
                  Register
                </Button>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </Section>
  );
}

const defaultFormValues = {
  name: '',
  email: '',
  password: '',
  password2: '',
};

const defaultErrorsValues = {
  name: '',
  email: '',
  password: '',
  password2: '',
};
