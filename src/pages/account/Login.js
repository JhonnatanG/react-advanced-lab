import React, { useState } from 'react';
import { useAuth } from './../../components/auth/useAuth';
import { Section, Row, Col, TextInput, Icon, Button } from 'react-materialize';
import history from './../../history';
import { DASHBOARD_URL } from './../../routes';
import gateway from './../../api/gateway';

function Login(props) {
  const [formValues, setFormValues] = useState(defaultFormValues);
  const [errors, setErrors] = useState(defaultErrorsValues);

  const onLoginClick = async () => {
    try {
      const loginResponse = await gateway.post('/users/login', formValues);
      const token = loginResponse.token;
      const payload = loginResponse.payload;
      props.authContext.login(payload, token);
      history.push(DASHBOARD_URL);
    } catch (err) {
      setErrors({ ...errors, ...err });
    }
  };

  const handleChange = (data) => {
    setFormValues({ ...formValues, ...data });
  };

  return (
    <Section className="container">
      <Row>
        <Col
          s={12}
          m={8}
          xl={4}
          offset="m2 xl4"
          className="z-depth-4 card-panel border-radius-6 login-card bg-opacity-8"
        >
          <div className="login-form">
            <Row>
              <h4 className="header center-align">Login</h4>
            </Row>
            <Row>
              <TextInput
                id="email"
                className={errors.email ? 'invalid' : ''}
                error={errors.email}
                icon={<Icon>email</Icon>}
                label="Email"
                s={12}
                value={formValues.email}
                onChange={(e) => {
                  handleChange({ email: e.target.value });
                }}
              />
            </Row>
            <Row>
              <TextInput
                id="password"
                className={errors.password ? 'invalid' : ''}
                error={errors.password}
                type="password"
                icon={<Icon>lock_outline</Icon>}
                label="Password"
                s={12}
                value={formValues.password}
                onChange={(e) => {
                  handleChange({ password: e.target.value });
                }}
              />
            </Row>
            <Row>
              <Col s={12} className="input-field">
                <Button
                  waves="light"
                  className="col s12"
                  onClick={onLoginClick}
                >
                  Login
                </Button>
              </Col>
              <span className="red-text">{errors.network}</span>
            </Row>
          </div>
        </Col>
      </Row>
    </Section>
  );
}

export default useAuth(Login);

const defaultFormValues = {
  email: '',
  password: '',
  network: '',
};

const defaultErrorsValues = {
  email: '',
  password: '',
  network: '',
};
