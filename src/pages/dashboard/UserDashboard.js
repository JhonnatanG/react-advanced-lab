import React from 'react';
import requireAuth from './../../components/auth/requireAuth';
import { Section, Row, Col, CardPanel } from 'react-materialize';

function UserDashboard(props) {
  const { currentUser } = props.authContext;
  return (
    <Section className="container">
      <Row>
        <Col s={12}>
          <h4 className="header center-align">User Dashboard</h4>
          <CardPanel>
            <Row>
              <Col>
                <div href="#!" className="avatar">
                  <img
                    src="https://www.managedhealthcareexecutive.com/sites/default/files/Technology%20Power%20Button_2.png"
                    alt="users view avatar"
                    className="z-depth-4 circle"
                    height="64"
                    width="64"
                  />
                </div>
              </Col>
              <Col>
                <h6 className="media-heading">
                  {/* Display the Name of the User */}
                  <span>{currentUser.name} </span>
                  <span className="grey-text"></span>
                  {/* Display the Email of the User */}
                  <span className="grey-text">{currentUser.email}</span>
                </h6>
                <span>ID: </span>
                {currentUser.id}
                <span></span>
              </Col>
            </Row>
          </CardPanel>
        </Col>
      </Row>
    </Section>
  );
}

export default requireAuth()(UserDashboard);
