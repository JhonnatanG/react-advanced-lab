import React from 'react';
import { useAuth } from './../auth/useAuth';
import { Navbar, NavItem, Button, Icon } from 'react-materialize';
import { LANDING_URL, LOGIN_URL, REGISTER_URL } from './../../routes';
import history from './../../history';

function Navigation(props) {
  const { auth, logout } = props.authContext;

  const onLogoutClick = () => {
    logout();
    history.push(LANDING_URL);
  };

  return (
    <Navbar
      centerChildren
      alignLinks="right"
      brand={
        <a className="brand-logo" href={LANDING_URL}>
          Lab Three
        </a>
      }
      id="mobile-nav"
      menuIcon={<Icon>menu</Icon>}
      options={{
        draggable: true,
        edge: 'left',
        inDuration: 250,
        onCloseEnd: null,
        onCloseStart: null,
        onOpenEnd: null,
        onOpenStart: null,
        outDuration: 200,
        preventScrolling: true,
      }}
    >
      {!auth ? <NavItem href={LOGIN_URL}>Login</NavItem> : ''}
      {!auth ? <NavItem href={REGISTER_URL}>Register</NavItem> : ''}
      {auth ? (
        <Button node="a" onClick={onLogoutClick}>
          Logout <Icon right>cloud</Icon>
        </Button>
      ) : (
        ''
      )}
    </Navbar>
  );
}

export default useAuth(Navigation);
