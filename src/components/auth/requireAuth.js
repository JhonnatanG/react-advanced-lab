import React from 'react';
import { useAuth } from './useAuth';
import history from './../../history';
const requireAuth = (props) => (WrappedComponent) => {
  class RequireAuth extends React.Component {
    componentDidMount() {
      this.shouldNavigateAway();
    }

    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    shouldNavigateAway() {
      if (!this.props.authContext.auth) {
        history.push('/login');
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  RequireAuth.displayName = `RequireAuth(${WrappedComponent.name})`;
  return useAuth(RequireAuth);
};

export default requireAuth;
