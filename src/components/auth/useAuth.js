import React from 'react';
import { AuthContext } from './AuthContext';
export function useAuth(Component) {
  return function AuthComponent(props) {
    return (
      <AuthContext.Consumer>
        {(contexts) => <Component {...props} {...contexts} />}
      </AuthContext.Consumer>
    );
  };
}
