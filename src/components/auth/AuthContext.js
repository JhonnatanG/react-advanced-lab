import * as React from 'react';
export const AuthContext = React.createContext({
  authContext: {
    isAuth: false,
    setIsAuth: () => {},
    currentUser: null,
    setCurrentUser: () => {},
    login: () => {},
    logout: () => {},
  },
});
