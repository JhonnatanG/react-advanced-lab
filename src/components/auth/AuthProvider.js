import React, { useState } from 'react';
import { AuthContext } from './AuthContext';
import { setAuthToken } from './../../api/gateway';

export default function AuthProvider(props) {
  const [auth, setAuth] = useState(false);
  const [currentUser, setCurrentUser] = useState({});

  const login = (currentUser, token) => {
    setAuthToken(token);
    localStorage.setItem('JWT_TOKEN_LAB3', token);
    setAuth(true);
    setCurrentUser(currentUser);
  };

  const logout = () => {
    setAuthToken(null);
    localStorage.removeItem('JWT_TOKEN_LAB3');
    setAuth(false);
    setCurrentUser(null);
  };

  return (
    <AuthContext.Provider
      value={{
        authContext: {
          auth,
          setAuth,
          currentUser,
          setCurrentUser,
          login,
          logout,
        },
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
}
