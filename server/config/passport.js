const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const User = mongoose.model('users');
const keys = require('../config/keys');

//Setup Options for JWT Strategy
const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: keys.secretOrKey,
};

module.exports = (passport) => {
  passport.use(
    //Define JWT Strategy
    new JwtStrategy(opts, (jwt_payload, done) => {
      //Get UserId from payload
      const userId = jwt_payload.id;
      //Try Find the User by Id to verify the Id is Valid
      User.findById(userId)
        .then((user) => {
          if (user) {
            return done(null, user);
          }
          return done(null, null);
        })
        .catch((err) => console.log(err));
    })
  );
};
